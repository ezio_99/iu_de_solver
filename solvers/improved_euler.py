from typing import List

from .exact import ExactSolver


class ImprovedEulerSolver(ExactSolver):
    def solve(self, n: int) -> List[float]:
        x = self.get_xs(n)
        y = [self.y0]
        step = self.get_step(n)

        for i in range(1, n):
            k1 = self.derivative(x[i - 1], y[i - 1])
            k2 = self.derivative(x[i], y[i - 1] + step * k1)
            yi = y[i - 1] + step / 2 * (k1 + k2)
            y.append(yi)

        return y
