from typing import List

from .exact import ExactSolver


class RungeKuttaSolver(ExactSolver):
    def solve(self, N: int) -> List[float]:
        x = self.get_xs(N)
        y = [self.y0]
        step = self.get_step(N)

        for i in range(1, N):
            k1 = self.derivative(x[i - 1], y[i - 1])
            k2 = self.derivative(x[i - 1] + step / 2, y[i - 1] + step / 2 * k1)
            k3 = self.derivative(x[i - 1] + step / 2, y[i - 1] + step / 2 * k2)
            k4 = self.derivative(x[i - 1] + step, y[i - 1] + step * k3)

            yi = y[i - 1] + 1 / 6 * step * (k1 + 2 * k2 + 2 * k3 + k4)
            y.append(yi)

        return y
