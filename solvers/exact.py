from math import sqrt
from typing import List


class ExactSolver:
    @staticmethod
    def derivative(x: float, y: float) -> float:
        return sqrt(y - x) / sqrt(x) + 1

    def get_constant(self) -> float:
        return -2
        # return min(1 - sqrt(self.y0 - self.x0), 1 + sqrt(self.y0 - self.x0))

    def exact(self, x: float) -> float:
        return 2 * x - 2 * self.C * sqrt(x) + self.C ** 2 + self.C2

    def __init__(self, x0, y0, X):
        self.x0 = x0
        self.y0 = y0
        self.X = X
        self.C = self.get_constant()
        self.C2 = y0 - (2 * x0 + 4 * sqrt(x0) + 4)

    def get_xs(self, N: int) -> List[float]:
        step = self.get_step(N)
        x = []
        for i in range(N):
            x.append(self.x0 + step * i)
        return x

    def get_step(self, N: int) -> float:
        return (self.X - self.x0) / N

    def solve(self, N: int) -> List[float]:
        x = self.get_xs(N)
        y = []
        for i in x:
            y.append(self.exact(i))
        return y

    def get_local_errors(self, N: int) -> List[float]:
        """Return array of absolute (<current_solver>.solve - exact.solve)."""
        exact = self.__solve(N)
        approximate = self.solve(N)
        result = []
        for i in range(len(exact)):
            result.append(abs(exact[i] - approximate[i]))
        return result

    def get_errors_changing(self, n0: int, Nf: int) -> List[float]:
        """Return array of maxs of local errors for each n in range of ns: n0 to Nf."""
        result = []
        for n in range(n0, Nf + 1):
            result.append(max(self.get_local_errors(n)))
        return result

    __solve = solve
