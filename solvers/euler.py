from typing import List

from .exact import ExactSolver


class EulerSolver(ExactSolver):
    def solve(self, N: int) -> List[float]:
        x = self.get_xs(N)
        y = [self.y0]
        step = self.get_step(N)

        for i in range(1, N):
            yi = y[i - 1] + step * self.derivative(x[i - 1], y[i - 1])
            y.append(yi)

        return y
