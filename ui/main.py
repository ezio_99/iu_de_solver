from abc import ABC, abstractmethod
from typing import Union

from PyQt5.QtWidgets import QDesktopWidget, QMessageBox
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
from matplotlib.figure import Figure

from solvers.exact import ExactSolver
from ui.design import Ui_MainWindow
from solvers.euler import EulerSolver
from solvers.improved_euler import ImprovedEulerSolver
from solvers.runge_kutta import RungeKuttaSolver

from PyQt5 import QtWidgets


class FloatValueError(ValueError):
    pass


class IntValueError(ValueError):
    pass


class NonNaturalNError(ValueError):
    pass


class x0XRangesError(ValueError):
    pass


class n0NfRangesError(ValueError):
    pass


class NonNaturaln0Error(ValueError):
    pass


class Colors:
    exact = '#000000'
    euler = '#ff0000'
    improved_euler = '#0000ff'
    runge_kutta = '#00ff00'


class Settings:
    window_title = "DE solver"

    solution_title = "Solutions"
    local_errors = "Local errors"
    errors_changing = "Errors changing"

    exact_title = "Exact"
    euler_title = "Euler"
    improved_euler_title = "Improved Euler"
    runge_kutta_title = "Runge-Kutta"

    x0 = 1
    y0 = 10
    X = 15
    N = 100
    n0 = 1
    Nf = 100


class BaseFigure(Figure, ABC):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.y0 = None
        self.x0 = None
        self.X = None
        self.N = None
        self.is_exact = True
        self.is_euler = True
        self.is_improved_euler = True
        self.is_runge_Kutta = True
        self.set_tight_layout("True")

    @abstractmethod
    def refresh(self):
        raise NotImplemented()


class SolutionFigure(BaseFigure):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.axes1 = self.add_subplot(111, title=Settings.solution_title)

    def refresh(self):
        x0 = self.x0
        y0 = self.y0
        X = self.X
        N = self.N

        exact_solver = ExactSolver(x0, y0, X)
        euler_solver = EulerSolver(x0, y0, X)
        improved_euler_solver = ImprovedEulerSolver(x0, y0, X)
        runge_kutta_solver = RungeKuttaSolver(x0, y0, X)
        x = exact_solver.get_xs(N)

        self.axes1.clear()
        self.axes1.set_title(Settings.solution_title)
        if self.is_exact:
            self.axes1.plot(x, exact_solver.solve(N), label=Settings.exact_title, color=Colors.exact)
        if self.is_euler:
            self.axes1.plot(x, euler_solver.solve(N), label=Settings.euler_title, color=Colors.euler)
        if self.is_improved_euler:
            self.axes1.plot(x, improved_euler_solver.solve(N), label=Settings.improved_euler_title,
                            color=Colors.improved_euler)
        if self.is_runge_Kutta:
            self.axes1.plot(x, runge_kutta_solver.solve(N), label=Settings.runge_kutta_title, color=Colors.runge_kutta)
        if self.is_exact or self.is_euler or self.is_improved_euler or self.is_runge_Kutta:
            self.axes1.legend()


class LocalErrorsFigure(BaseFigure):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.axes1 = self.add_subplot(111, title=Settings.local_errors)

    def refresh(self):
        x0 = self.x0
        y0 = self.y0
        X = self.X
        N = self.N

        euler_solver = EulerSolver(x0, y0, X)
        improved_euler_solver = ImprovedEulerSolver(x0, y0, X)
        runge_kutta_solver = RungeKuttaSolver(x0, y0, X)
        x = euler_solver.get_xs(N)

        self.axes1.clear()
        self.axes1.set_title(Settings.local_errors)

        if self.is_euler:
            self.axes1.plot(x, euler_solver.get_local_errors(N), label=Settings.euler_title, color=Colors.euler)
        if self.is_improved_euler:
            self.axes1.plot(x, improved_euler_solver.get_local_errors(N),
                            label=Settings.improved_euler_title, color=Colors.improved_euler)
        if self.is_runge_Kutta:
            self.axes1.plot(x, runge_kutta_solver.get_local_errors(N),
                            label=Settings.runge_kutta_title, color=Colors.runge_kutta)
        if self.is_euler or self.is_improved_euler or self.is_runge_Kutta:
            self.axes1.legend()


class ErrorsChangingFigure(BaseFigure):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.n0 = None
        self.Nf = None
        self.axes1 = self.add_subplot(111, title=Settings.errors_changing)

    def refresh(self):
        x0 = self.x0
        y0 = self.y0
        X = self.X

        euler_solver = EulerSolver(x0, y0, X)
        improved_euler_solver = ImprovedEulerSolver(x0, y0, X)
        runge_kutta_solver = RungeKuttaSolver(x0, y0, X)

        self.axes1.clear()
        self.axes1.set_title(Settings.errors_changing)
        n0 = self.n0
        Nf = self.Nf
        x = list(range(n0, Nf + 1))
        if self.is_euler:
            self.axes1.plot(x, euler_solver.get_errors_changing(n0, Nf), label=Settings.euler_title, color=Colors.euler)
        if self.is_improved_euler:
            self.axes1.plot(x, improved_euler_solver.get_errors_changing(n0, Nf),
                            label=Settings.improved_euler_title, color=Colors.improved_euler)
        if self.is_runge_Kutta:
            self.axes1.plot(x, runge_kutta_solver.get_errors_changing(n0, Nf), label=Settings.runge_kutta_title,
                            color=Colors.runge_kutta)
        if self.is_euler or self.is_improved_euler or self.is_runge_Kutta:
            self.axes1.legend()


class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self):
        super().__init__()
        super().setupUi(self)

        self.n0_lbl_pos = self.verticalLayoutWidget_4.pos()
        self.calculate_btn_pos = self.calculate_btn.pos()

        self.setup_ui()
        self.set_handlers()

    def setup_ui(self):
        self.tabWidget.setCurrentIndex(0)
        self.setWindowTitle(Settings.window_title)

        self.x0_le.setText(str(Settings.x0))
        self.y0_le.setText(str(Settings.y0))
        self.X_le.setText(str(Settings.X))
        self.N_le.setText(str(Settings.N))
        self.n0_le.setText(str(Settings.n0))
        self.Nf_le.setText(str(Settings.Nf))

        self.solution_figure = SolutionFigure()
        self.solution_canvas = FigureCanvas(self.solution_figure)
        self.canvas1_lt.addWidget(NavigationToolbar(self.solution_canvas, self))
        self.canvas1_lt.addWidget(self.solution_canvas)

        self.local_errors_figure = LocalErrorsFigure()
        self.local_errors_canvas = FigureCanvas(self.local_errors_figure)
        self.canvas1_lt.addWidget(NavigationToolbar(self.local_errors_canvas, self))
        self.canvas1_lt.addWidget(self.local_errors_canvas)

        self.errors_changing_figure = ErrorsChangingFigure()
        self.range_max_errors_canvas = FigureCanvas(self.errors_changing_figure)
        self.canvas2_lt.addWidget(NavigationToolbar(self.range_max_errors_canvas, self))
        self.canvas2_lt.addWidget(self.range_max_errors_canvas)

        self.hide_tab2_controls()
        self.calculate_btn.move(self.n0_lbl_pos)

        self.setFixedSize(self.size())
        self.center_window(self)

    def hide_tab2_controls(self):
        self.verticalLayoutWidget_4.hide()
        self.verticalLayoutWidget_4.move(10 ** 9, 10 ** 9)

    def show_tab2_controls(self):
        self.verticalLayoutWidget_4.show()
        self.verticalLayoutWidget_4.move(self.n0_lbl_pos)

    @staticmethod
    def center_window(window):
        frameGm = window.frameGeometry()
        center_point = QDesktopWidget().availableGeometry().center()
        frameGm.moveCenter(center_point)
        window.move(frameGm.topLeft())

    def create_dialog(self, text, icon=QMessageBox.Warning):
        dialog = QMessageBox(self)
        dialog.setIcon(icon)

        dialog.setText(text)
        dialog.setWindowTitle("Warning")
        dialog.setStandardButtons(QMessageBox.Ok)

        dialog.setModal(True)
        MainWindow.center_window(dialog)
        dialog.show()

    def tab_changed(self):
        if self.tabWidget.currentIndex() == 0:
            self.hide_tab2_controls()
            self.calculate_btn.move(self.n0_lbl_pos)
        else:
            self.show_tab2_controls()
            self.calculate_btn.move(self.calculate_btn_pos)

    def set_handlers(self):
        self.tabWidget.currentChanged.connect(self.tab_changed)

        # checkboxes
        self.euler_cbx.stateChanged.connect(self.calculate)
        self.improved_euler_cbx.stateChanged.connect(self.calculate)
        self.runge_cutta_cbx.stateChanged.connect(self.calculate)
        self.exact_cbx.stateChanged.connect(self.calculate)

        # buttons
        self.calculate_btn.clicked.connect(self.calculate)

    def get_params_for_tab1_calculations(self, figure: Union[SolutionFigure, LocalErrorsFigure, ErrorsChangingFigure]):
        try:
            figure.x0 = float(self.x0_le.text())
            figure.y0 = float(self.y0_le.text())
            figure.X = float(self.X_le.text())
        except ValueError as e:
            raise FloatValueError from e

        try:
            figure.N = int(self.N_le.text())
        except ValueError as e:
            raise IntValueError from e

        figure.is_exact = self.exact_cbx.isChecked()
        figure.is_euler = self.euler_cbx.isChecked()
        figure.is_improved_euler = self.improved_euler_cbx.isChecked()
        figure.is_runge_Kutta = self.runge_cutta_cbx.isChecked()

        if figure.N < 2:
            raise NonNaturalNError()

        if figure.X <= figure.x0:
            raise x0XRangesError()

    def get_params_for_tab2_calculations(self, figure: ErrorsChangingFigure):
        self.get_params_for_tab1_calculations(figure)

        try:
            figure.n0 = int(self.n0_le.text())
            figure.Nf = int(self.Nf_le.text())
        except ValueError as e:
            raise IntValueError from e

        if figure.n0 < 1:
            raise NonNaturaln0Error()

        if figure.Nf <= figure.n0:
            raise n0NfRangesError()

    def calculate(self):
        try:
            self.get_params_for_tab1_calculations(self.solution_figure)
            self.get_params_for_tab1_calculations(self.local_errors_figure)
            self.get_params_for_tab2_calculations(self.errors_changing_figure)
            if self.tabWidget.currentIndex() == 0:
                self.solution_figure.refresh()
                self.solution_canvas.draw()
                self.local_errors_figure.refresh()
                self.local_errors_canvas.draw()
            else:
                self.errors_changing_figure.refresh()
                self.range_max_errors_canvas.draw()
        except FloatValueError:
            self.create_dialog("x0, X, y should be valid float")
        except IntValueError:
            if self.tabWidget.currentIndex() == 0:
                self.create_dialog("N should be valid int")
            else:
                self.create_dialog("n0, Nf should be valid int")
        except NonNaturalNError:
            self.create_dialog("N should be greater than 1")
        except x0XRangesError:
            self.create_dialog("x0 should be less than X")
        except NonNaturaln0Error:
            self.create_dialog("n0 should be greater than 0")
        except n0NfRangesError:
            self.create_dialog("n0 should be less than Nf")
        except ZeroDivisionError as e:
            print(e)
            self.create_dialog("Given interval should have no discontinuities")
        except ValueError as e:
            if str(e) == "math domain error":
                print(e)
                self.create_dialog("Given interval should have no discontinuities")
            else:
                raise e
        except Exception as e:
            print(e)
            self.create_dialog("Unknown error")
