Info
====
This is repository related to Computational Practicum, Differential Equations course, Fall 2020, Innopolis University.

Install dependencies
====================
    pip install -r requirements.txt

Usage
=====
    python3 main.py
